<footer class="footer">
			<div class="container">
				<div class="row align-items-center flex-row-reverse">
					<div class="col-md-12 col-sm-12 mt-3 mt-lg-0 text-center">
						Copyright © <?php echo date('Y'); ?> <a href="http://mubisys.com.br" target="_blank">Mubisys </a>. Designed por <a href="#">Demian Escobar</a>
					</div>
				</div>
			</div>
		</footer>