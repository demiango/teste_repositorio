<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="msapplication-TileColor" content="#f96e3c">
	<meta name="theme-color" content="#f96e3c">
	<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<link rel="icon" href="favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />

	<!-- Title -->
	<title>IxopPlay - Rota de fuga para quem quer rota de compra</title>
	<link rel="stylesheet" href="assets/fonts/fonts/font-awesome.min.css">

	<!-- Font Family-->
	<link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700&amp;subset=latin-ext,vietnamese" rel="stylesheet">

	<!-- Sidemenu Css -->
	<link href="assets/plugins/fullside-menu/css/style.css" rel="stylesheet" />
	<link href="assets/plugins/fullside-menu/waves.min.css" rel="stylesheet" />

	<!-- Bootstrap Css -->
	<link href="assets/plugins/bootstrap-4.1.3/css/bootstrap.min.css" rel="stylesheet" />

	<!-- Dashboard Css -->
	<link href="assets/css/dashboard.css" rel="stylesheet" />

	<!-- JQVMap -->
	<link href="assets/plugins/jqvmap/jqvmap.min.css" rel="stylesheet" />

	<!-- Morris.js Charts Plugin -->
	<link href="assets/plugins/morris/morris.css" rel="stylesheet" />

	<!-- Custom scroll bar css-->
	<link href="assets/plugins/scroll-bar/jquery.mCustomScrollbar.css" rel="stylesheet" />

	<!---Font icons-->
	<link href="assets/plugins/iconfonts/plugin.css" rel="stylesheet" />
</head>

<body>
	<div id="loading"></div>
	<div class="page">
		<div class="page-main">
			<div class="app-header1 header py-1 d-flex">
				<div class="container-fluid">
					<div class="d-flex">
						<a class="header-brand" href="index.html">
							<img src="assets/images/brand/logo.png" class="header-brand-img" alt="Uplor  logo">
						</a>
						<div class="menu-toggle-button">
							<a class="nav-link wave-effect" href="#" id="sidebarCollapse">
								<span class="fa fa-bars"></span>
							</a>
						</div>
						<div>
							<!-- <div class="searching">
								<a href="javascript:void(0)" class="search-open searching1">
									<i class="fa fa-search"></i>
								</a>
								<div class="search-inline">
									<form>
										<input type="text" class="form-control" placeholder="Searching...">
										<button type="submit">
											<i class="fa fa-search"></i>
										</button>
										<a href="javascript:void(0)" class="search-close">
											<i class="fa fa-times"></i>
										</a>
									</form>
								</div>
							</div> -->
						</div>
						<div class="d-flex order-lg-2 ml-auto">
							<div class="dropdown d-none d-md-flex">
								<a class="nav-link icon full-screen-link">
									<i class="fe fe-maximize floating" id="fullscreen-button"></i>
								</a>
							</div>




						</div>
					</div>
				</div>
			</div>