<div class=" content-area ">
	<div class="page-header">
		<h4 class="page-title">Painel MubiSys</h4>
	</div>

	<div class="row">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-header">
					<div class="card-title">Usuários</div>
					<div class="card-options">
						<a href="#" class="card-options-collapse" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a>
						<a href="#" class="card-options-remove" data-toggle="card-remove"><i class="fe fe-x"></i></a>
					</div>
				</div>
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-bordered border mb-0 align-items-center">
							<thead>
								<tr>
									<th>Id</th>
									<th>Nome</th>
									<th>E-mail</th>
									<th>Data</th>
									<th>Java WEb Token (JWT)</th>

								</tr>
							</thead>
							<tbody>

								<?php
								for ($x = 0; $x < sizeof($_SESSION['tds_usuarios']); $x++) {
								?>
									<tr>
										<td><?php echo $_SESSION['tds_usuarios'][$x]['id'] == $_SESSION['usuario']['id'] ? '<font color="red">' . $_SESSION['tds_usuarios'][$x]['id'] . '</font>' : $_SESSION['tds_usuarios'][$x]['id'] ?></td>
										<td><?php echo $_SESSION['tds_usuarios'][$x]['id'] == $_SESSION['usuario']['id'] ? '<font color="red">' . $_SESSION['tds_usuarios'][$x]['nome'] . '</font>' : $_SESSION['tds_usuarios'][$x]['nome'] ?></td>
										<td><?php echo $_SESSION['tds_usuarios'][$x]['id'] == $_SESSION['usuario']['id'] ? '<font color="red">' . $_SESSION['tds_usuarios'][$x]['email'] . '</font>' : $_SESSION['tds_usuarios'][$x]['email'] ?></td>
										<td><?php echo $_SESSION['tds_usuarios'][$x]['id'] == $_SESSION['usuario']['id'] ? '<font color="red">' . $_SESSION['tds_usuarios'][$x]['senha'] . '</font>' : $_SESSION['tds_usuarios'][$x]['senha'] ?></td>
										<td><?php echo $_SESSION['tds_usuarios'][$x]['id'] == $_SESSION['usuario']['id'] ? '<font color="red">' . $_SESSION['tds_usuarios'][$x]['token'] . '</font>' : $_SESSION['tds_usuarios'][$x]['token'] ?></td>
									</tr>
								<?php } ?>


							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>



</div>